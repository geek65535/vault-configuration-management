# Vault configuration management

This is a based on a sample repository associated with the blog post
["Codifying Vault Policies and Configuration"][post] on the HashiCorp blog. For
more information, please read that blog post.

[post]: https://www.hashicorp.com/blog/codifying-vault-policies-and-configuration.html
There is another project called [Vaultsmith](https://github.com/starlingbank/vaultsmith) based on that same blog post that takes a similar approach, but is written in Go.
I haven't looked into it deeply, but provide the link here for general interest.

The script 'manage-vault.sh' can be run with the '--help' or '--usage' options for more details
on how to run it.

The script follows the structure of a data directory ('vault-data' in the current
directory by default) and translates the directory paths into paths for the API. Files in those
directories are considered to be endpoints for the API, and the contents are JSON-formated
configuration data.  
Any file with valid data will be used to configure vault. Any file with the extension of '.REMOVE'
will be used to remove/delete that configuration. So if you have the file:  
vault-data/sys/policy/somepolicy.REMOVE,  
then the policy named 'somepolicy' will be removed from the vault configuration. This allows the
original contents of the file to be preserved for testing or reference.
 
NB: files can have the extension .json, but the script will strip the extension if it is there, so
bare filenames are OK (eg goodpolicy instead of goodpolicy.json)

The configuration file (by default, 'vault.conf' in the data directory) contains a list of paths
to traverse, and the order to traverse them.  
This is necessary because you might, for example, have to configure the ldap authentication method
(in sys/auth/ldap) before you can configure it (in auth/ldap/config and auth/ldap/groups/somegroup).

The data directory will look something like this:  
vault-data/  
├── auth  
│   ├── ldap  
│   │   ├── config.json  
│   │   └── groups  
│   │       └── prod-login.json  
│   └── userpass  
│       └── users  
│           ├── user1ro.json  
│           ├── user1rw.json  
│           ├── user1rw.json-orig  
│           ├── user2ro.json  
│           └── user2rw.json  
├── sys  
│   ├── auth  
│   │   ├── github  
│   │   ├── ldap.json  
│   │   └── userpass.json  
│   ├── license  
│   ├── mounts  
│   │   ├── safe1.json  
│   │   └── safe2.json  
│   └── policy  
│       ├── policy-manager.json  
│       ├── prod-login-policy.json  
│       ├── safe1ro.json  
│       ├── safe1rw.json  
│       ├── safe2ro.json  
│       └── safe2rw.json  
└── vault.conf  


Vault can also be managed through ansible and through Terraform. Unfortunately, both of those methods
are somewhat incomplete as of now (Sep 2018). It may be worth revisiting them in the future.  
The advantage to using this script for configuration is that it uses the API, so literally everything
in vault can be configured using this method.

All documentation for the API can be found here: https://www.vaultproject.io/api/index.html  
It's generally worth reading the main docs for more detail: https://www.vaultproject.io/docs/index.html

This repo includes three directories:
vault-data: live/production configuration
test-data: testing config. make changes here, test, copy to vault-data when ready
sample-data: demo / testing the script
