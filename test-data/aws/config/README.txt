This directory should contain a file called 'root.json', with contents similar to:
{
  "access_key": "XXXXXXXXXXXXXXXXXXXX",
  "secret_key": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
  "region": "us-east-1"
}

This file should *not* be committed to the repo, as it contains very sensitive data--access to AWS.
Go to https://console.aws.amazon.com/iam/home#/home and create a user. Create and assign
a profile similar to https://www.vaultproject.io/docs/secrets/aws/index.html#example-iam-policy-for-vault
Give the user access type of "programmatic access." That will allow AWS to generate the access
and secret keys needed for the 'root' credentials. As the docs say, do *not* use actual
root credentials here!

