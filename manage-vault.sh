#!/bin/bash

# copyright 2019 Paul Archer
# based on https://www.hashicorp.com/blog/codifying-vault-policies-and-configuration.html



#=========================== process options ============================
# get config file with paths to operate on
# and data directory
ARGUMENT_LIST=(
    "config-file"
    "data-dir"
)
opts=$(getopt \
    --longoptions "config-file:,data-dir:,help,usage,dev,suppress-warnings,verbose" \
    --name "$(basename "$0")" \
    --options "" \
    -- "$@"
)
#    --longoptions "$(printf "%s:," "${ARGUMENT_LIST[@]}"),help,usage,dev,suppress-warnings" \

function usage(){
  echo "Usage: $0 [--data-dir <dir> ] [ --config-file <file> ] [ --dev ][ --suppress-warnings ]
  Defaults to data directory of ./vault-data
  and configuration file of vault.conf inside the data directory.

  Any files with no extension or with the extension of .json are assumed
  to be valid configuration files, and the configuration contained in the file
  will be used to configure the endpoint specified by the directory path+filename.
  For example, the file <data-dir>/sys/auth/ldap.json will be used to configure
  the LDAP authentication endpoint.
  Any file with the extenstion of .REMOVE will signal that the configuration at
  the endpoint specified by the directory path+filename will be removed. The 
  contents of the file will not be considered, and so can hold the original
  configuration for reference.
  Files named README.txt or README.md will be skipped.

  This script relies on the external commands: curl, jq, and readlink.
  It also assumes that the environmental variables
  VAULT_ADDR and VAULT_TOKEN have been set, unless the --dev option is used,
  in which case the variables will be set to:
  VAULT_ADDR=http://127.0.0.1:8200
  VAULT_TOKEN=root 
  ( vault server -dev -dev-root-token-id=root )
  
  --suppress-warnings will remove the most common warnings, usually seen when a
  path or mount is already configured/mounted. These errors are normal when running
  the script multiple times.
  
  --verbose will echo out the curl command being run. Warning: this will include the token being used."
  exit 1
}
eval set --$opts
while [[ $# -gt 0 ]]; do
    case "$1" in
        --data-dir)
            datadir=$2
            shift 2
            ;;

        --config-file)
            configfile=$2
            shift 2
            ;;
        --dev)
            DEV=true
            shift
            ;;
        --verbose)
            VERBOSE=true
            shift
            ;;
        --suppress-warnings)
            SUPPRESS_WARNINGS=true
            shift
            ;;
        --)
            shift
            ;;
        *)
            usage
            ;;
    esac
done

# assume defaults if nothing on command line
datadir=${datadir:-./vault-data}
configfile=${configfile:-$datadir/vault.conf}
# convert to absolute path to avoid issues later
configfile=$(readlink -f $configfile)


if ! [[ -d $datadir ]]
  then  echo "$datadir doesn't exist or isn't a directory"
    exit 1
  fi
if ! [[ -f $configfile ]]
  then  echo "$configfile doesn't exist"
    exit 1
  fi

#=========================== setup functions ============================
function provision() {
  path=$1
  file=$2
  shortfile=${2%.json}
  if [[ "$VERBOSE" == true ]]; then
   echo curl --show-error --header \"X-Vault-Token: ${VAULT_TOKEN}\" --data @"$datadir/$path/$file" "${VAULT_ADDR}/v1/$path/$shortfile" 
 fi
    provision_error_msg=$(curl \
    --silent \
    --show-error \
    --header "X-Vault-Token: ${VAULT_TOKEN}" \
    --data @"$file" \
    "${VAULT_ADDR}/v1/$path/$shortfile" \
    | jq -r  .errors[] 2>/dev/null | sed -e '/error occurred/d' -e '/^$/d'
    )
}

function deprovision() {
  path=$1
  file=$2
  shortfile=${2%.REMOVE}
    deprovision_error_msg=$(curl \
    --silent \
    --show-error \
    --header "X-Vault-Token: ${VAULT_TOKEN}" \
    --request DELETE \
    "${VAULT_ADDR}/v1/$path/$shortfile" \
    | jq -r  .errors[] | sed -e '/error occurred/d' -e '/^$/d'
    )
}

# for testing
# vault server -dev -dev-root-token-id=root
if [[ "$DEV" == true ]]; then
  export VAULT_ADDR=http://127.0.0.1:8200
  export VAULT_TOKEN=root
fi

#=========================== main loop ============================
# iterate over paths in config file,
# cd to each dir
config=$(cat $configfile)
cd $datadir
while IFS= read -r dir
do 
  if ! [[ -d "$dir" ]]
  then continue #skip line
  fi

  pushd $dir > /dev/null
  for file in $(ls)
  do
    if [[ -z $file || -d $file ]] ; then
      true # skip empty files and directories

    elif echo $file | egrep 'README.(txt|md)' >/dev/null; then
      true # skip README files

    elif echo $file | grep 'REMOVE$' >/dev/null ; then

      deprovision $dir $file
      if [[ -n $deprovision_error_msg ]] ;then 
        echo $dir/$file: $deprovision_error_msg 
      fi

    elif cat $file | jq . >/dev/null 2>&1; then
      provision $dir $file
      if [[ -n $provision_error_msg ]] ;then
        if [[ -n $SUPPRESS_WARNINGS ]]; then
          echo $dir/$file: $provision_error_msg | egrep -v 'path is already in use|path already in use|existing mount at '
        else
          echo $dir/$file: $provision_error_msg
        fi

      fi

    else echo "problem with $dir/$file. Skipping"
    fi
  done
  popd >/dev/null # pushd $dir
done < $configfile

exit 0
